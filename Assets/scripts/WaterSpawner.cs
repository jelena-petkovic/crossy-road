﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSpawner : MonoBehaviour
{

    [SerializeField] private GameObject vehicle;
    [SerializeField] private Transform spawnPos;

    [SerializeField] private float minSeparationTime;
    [SerializeField] private float maxSeparationTime;

    public static bool trueOrFalse;
    void Start()
    {
        StartCoroutine(SpawnVehicle());      
    }

    private IEnumerator SpawnVehicle()
    {   
        while(true){
            yield return new WaitForSeconds(Random.Range(minSeparationTime,maxSeparationTime));
            
             GameObject w = Instantiate(vehicle, spawnPos.position ,Quaternion.identity);

            w.GetComponent<Wood>().directionX = 1;              
        
        }
    }

}
