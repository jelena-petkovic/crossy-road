﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodCollider : MonoBehaviour
{
    public static int polje = 0;

    void OnTriggerEnter(Collider other) {
        if(other.GetComponent<Collider> ().tag == "Right"){
            polje = 1;
        }
        if(other.GetComponent<Collider> ().tag == "Left"){
            polje = 2;
        }
        if(other.GetComponent<Collider> ().tag == "Up"){
            polje = 3;
        }
    }

    private void OnTriggerExit(Collider other) {
        polje = 0;
    }
}
