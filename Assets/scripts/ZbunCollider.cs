﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZbunCollider : MonoBehaviour
{
    // Start is called before the first frame update
   void OnTriggerEnter(Collider other) {
        if(other.GetComponent<Collider> ().tag == "Right"){
            WoodCollider.polje = 1;
        }
        if(other.GetComponent<Collider> ().tag == "Left"){
            WoodCollider.polje = 2;
        }
        if(other.GetComponent<Collider> ().tag == "Up"){
            WoodCollider.polje = 3;
        }
    }

    private void OnTriggerExit(Collider other) {
        WoodCollider.polje = 0;
    }
}
