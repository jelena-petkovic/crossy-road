﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class PlayerCollision : MonoBehaviour
{
    public ParticleSystem deathParticles;
     Vector3 _shakeIntensity = new Vector3(1,1,0) * .25f;
    public Camera mainCamera;
    public PlayerMovement movement;
    public Explosion expl;

    void OnCollisionEnter(Collision other) {
        if(other.collider.tag == "Water"){
            Death();
        }
        if(other.collider.GetComponent<Wood>() != null)
        {
            if(other.collider.GetComponent<Wood>().isLog)
            {
                transform.parent = other.collider.transform;
            }
        }else
        {
            transform.parent = null;
        }
    }
 
    void Death()
    {
            Tween.Shake(mainCamera.transform, mainCamera.transform.position , _shakeIntensity, 1f, 0);
            deathParticles.transform.parent = null;
            deathParticles.Play();
            FindObjectOfType<GameOver>().DeathSound();
            movement.enabled = false;
            GameManager.instance.Invoke("EndGame",1.5f);

    }


    void OnTriggerEnter(Collider other) {
         if(other.GetComponent<Collider> ().tag == "Car"){
            expl.explode();
            Death();
        }
    }

}
