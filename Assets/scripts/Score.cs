﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour
{
    private TextMeshProUGUI scoreText;

    private int score = 0;
    
     void OnEnable(){
        scoreText = GetComponent<TextMeshProUGUI>();
        PlayerMovement.Swiped += AddScore;
    }

    void OnDisable(){
        PlayerMovement.Swiped -= AddScore;
    }


    // Update is called once per frame
    void AddScore()
    {
        score++;
        scoreText.text = "SCORE: " + score.ToString();
    }
}
