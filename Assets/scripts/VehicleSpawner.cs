﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleSpawner : MonoBehaviour
{

    [SerializeField] private GameObject vehicle;
    [SerializeField] private Transform spawnPos;


    [SerializeField] private float minSeparationTime;
    [SerializeField] private float maxSeparationTime;

    public bool trueOrFalse;
    void Start()
    {
        StartCoroutine(SpawnVehicle());      
    }

    private IEnumerator SpawnVehicle()
    {   
        while(true){
                yield return new WaitForSeconds(Random.Range(minSeparationTime,maxSeparationTime));
                
                GameObject g = Instantiate(vehicle, spawnPos.position ,Quaternion.identity);
                    g.GetComponent<Vehicle>().directionX = -1;
        }
    }

}
