﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    public AudioSource death;

    public void DeathSound()
    {
        death.Play();
    }
}
