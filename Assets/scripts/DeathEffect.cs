﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathEffect : MonoBehaviour
{
    public ParticleSystem death;

    public void Deatheffect()
    {
        death.Play();
    }
}
