﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wood : MonoBehaviour
{
   [SerializeField] private float minSpeed;
   [SerializeField] private float maxSpeed;
   private float speed;

    public bool isLog;
    public float directionX = -1;


    void Start() {
        speed = Random.Range(minSpeed,maxSpeed);
    }
    private void Update()
    {   
         transform.Translate(new Vector3(directionX,0,0) * speed * Time.deltaTime);
   }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "EndWood")
            Destroy(gameObject);
    }
}
