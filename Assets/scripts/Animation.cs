﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation : MonoBehaviour
{
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }
    void OnEnable(){
        PlayerMovement.Swiped += Jump;
    }

    void OnDisable(){
        PlayerMovement.Swiped -= Jump;
    }
    // Update is called once per frame
    void Jump()
    {
        
        anim.SetTrigger("Jump");
    }
    void ResetJump(){
        anim.ResetTrigger("Jump");
    }
}
