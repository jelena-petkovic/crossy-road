﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    bool gameHasEnded = false;
    public GameObject gameOverText;
    public static GameManager instance;

    public GameObject scoreText;

    public GameObject image;


    void Awake() {
        if(instance == null)
            instance = this;
        else if(instance != this)
        {
            Destroy(gameObject);
        }    
    }

    void Update() {
        if(gameHasEnded == true && Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void EndGame(){

        if(gameHasEnded == false)
        {
            gameHasEnded = true;
        }

        scoreText.SetActive(false);
        gameOverText.SetActive(true);
        image.SetActive(true);
    }

}
