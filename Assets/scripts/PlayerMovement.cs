using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class PlayerMovement : MonoBehaviour
{

        public delegate void SwipedEventHandler();
        public static event SwipedEventHandler Swiped;
        public ParticleSystem moveParticles;

        public Rigidbody rb;
        Vector3 poz1;
        Vector3 poz2;
        float napred = 0.5f;
     void Update()
    {
            if(PaueMenu.gameIsPaused)
                return;

            if(Input.GetMouseButtonDown(0)){
                poz1 = Input.mousePosition;
            }
            if(Input.GetMouseButtonUp(0)){

                poz2 = Input.mousePosition;
        
                if((poz2-poz1).y > napred)
                {
                    if(WoodCollider.polje == 3)
                        return;
                    
                    moveParticles.Play();
                    Tween.Position(transform,rb.position,rb.position + new Vector3(0,0,1),0.2f,0);
                    OnSwiped();
                }else{

                    if (Input.mousePosition.x < Screen.width/2)
                    {
                        if(WoodCollider.polje == 2)
                            return;
                        
                        Tween.Position(transform,rb.position,rb.position + new Vector3(-1,0,0),0.2f,0);
                    }
                    else if (Input.mousePosition.x > Screen.width/2)
                    {   
                        if(WoodCollider.polje == 1)
                            return;
                        
                        Tween.Position(transform,rb.position,rb.position + new Vector3(1,0,0),0.2f,0);
                    }
                }     
            }   
    }

    void OnSwiped(){
        if(Swiped != null)
            Swiped();
    }
}
