﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : MonoBehaviour
{
    [SerializeField] private float minSpeed;
    [SerializeField] private float maxSpeed;
    //public static bool isDestroyed = false;
    private float speed;

    public float directionX = -1;

    void Start() {
        speed = Random.Range(minSpeed,maxSpeed);
    }

    private void Update()
    {   
        transform.Translate(new Vector3(directionX,0,0) * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "End"){
            Destroy(gameObject);
        }
        if(other.gameObject.tag == "CarSpeedCollider")
        {
            speed = other.gameObject.GetComponentInParent<Vehicle>().speed;
        }
    }
}
