﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveUpParticles : MonoBehaviour
{
    // Start is called before the first frame update
    public ParticleSystem moveUp;

    public void MoveEffect()
    {
        moveUp.Play();
    }
}
