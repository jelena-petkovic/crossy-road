﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GenerationLevel : MonoBehaviour
{

    public GameObject[] Water;
    public GameObject[] Road;
    public GameObject[] Grass;

    int firstRandom;
    int secondRandom;
    int disPlayer = 12;
    int direction = 1;
    int directionCar = 1;


    Vector3 intPos = new Vector3(0,0,0);

    void OnEnable(){
        PlayerMovement.Swiped += NewLayer;
    }

    void OnDisable(){
        PlayerMovement.Swiped -= NewLayer;
    }

    void NewLayer(){
            firstRandom = UnityEngine.Random.Range(1,4);
            if(firstRandom == 1){
                secondRandom = UnityEngine.Random.Range(1,3);
                int whichGrass;
                for(int i =0; i < secondRandom; i++){
                    
                    System.Random rnd = new System.Random();
                    whichGrass  = rnd.Next(0, 9); 

                    intPos = new Vector3(0,0,disPlayer);
                    disPlayer++;
                    GameObject GrassInst = Instantiate(Grass[whichGrass]) as GameObject;
                    GrassInst.transform.position = intPos;
                }
            }
            if(firstRandom == 2){
                secondRandom = UnityEngine.Random.Range(1,3);
                for(int i =0; i < secondRandom; i++){

                    intPos = new Vector3(0,0,disPlayer);
                    disPlayer++;
                    GameObject WaterInst = Instantiate(Water[direction]) as GameObject;
                    WaterInst.transform.position = intPos;
                    if(direction == 1)
                        direction = 0;
                    else
                    {
                        direction = 1;
                    }
                }
            }
            if(firstRandom == 3){
                
                secondRandom = UnityEngine.Random.Range(1,3);
                for(int i =0; i < secondRandom; i++){
            
                    intPos = new Vector3(0,0,disPlayer);
                    disPlayer++;
                    GameObject RoadInst = Instantiate(Road[directionCar]) as GameObject;
                    RoadInst.transform.position = intPos;

                    if(directionCar == 1)
                        directionCar = 0;
                    else
                    {
                        directionCar = 1;
                    }
                }
            }
    }

    
}
